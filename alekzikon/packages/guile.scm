(define-module (alekzikon packages guile)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages texinfo)
  #:use-module (guix build-system guile)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))


(define-public guile-inutil
  (package
   (name "guile-inutil")
   (version "0.2.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.com/alekzikon/guile-inutil.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "06qz6sm2d5wfg8hxvvkbg4q6zjgck9cla7hi6v463qykkywv49ni"))))
   (build-system guile-build-system)
   (native-inputs
    `(("guile" ,guile-2.2)
      ("texinfo" ,texinfo)))
   (home-page "https://gitlab.com/alekzikon/guile-inutil")
   (synopsis "Guile Inutil Library")
   (description
    "Example library I use to learn how to package Guile libraries for
the Guix System.")
   (license license:unlicense)))
