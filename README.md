# Experimental Guix Channel

This is a [channel][1] I use to learn packaging for the [Guix System][2].
This channel provides useless packages, don't use it.


[1]: https://guix.gnu.org/manual/en/html_node/Channels.html#Channels
[2]: https://guix.gnu.org/
